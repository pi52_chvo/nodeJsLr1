var express = require('express');
var bodyParser = require("body-parser");
var pageRender = require("pageCreator");
var request = require("request");
var app = new express();

var urlencodedParser = bodyParser.urlencoded({extended: false});

app.use(express.static(__dirname + "/public/"));

app.get("/", (req, res) => {
	res.send(pageRender.renderPage({
		name: "index.pug",
	}));
});

app.post("/getWeather", urlencodedParser, (req, res) => {
	var lat = req.body.lat;
	var lon = req.body.lon;
	var url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat.toString() +"&lon=" + lon.toString() + "&APPID=f873195bdc96541fbf815910dc18f562";
	request(url, (err, resp, body) => {
		if (!err && resp.statusCode === 200) {
			var info = JSON.parse(body);
			console.info(info);
			var page = pageRender.renderPage({
				name: "weather.pug",
				params: {
					country: info.sys.country,
					name: info.name,
					wSpeed: info.wind.speed,
					humidity: info.main.humidity,
					temp: Math.round(info.main.temp - 273.15),
					lon: info.coord.lon,
					lat: info.coord.lat
				}
			})
    	}
    	res.send(page);
	});
});

app.post("/save", urlencodedParser, (req, res) => {
	console.info(req.body);
	res.end();
});

app.listen(3000);